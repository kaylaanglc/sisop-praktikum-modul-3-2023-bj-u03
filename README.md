# Praktikum Sisop Modul 3 (BJ-U03)
Group Members:
1. Ahmad Danindra Nugroho (5025211259)
2. Mardhatillah Shevy Ananti (5025211070)
3. Raden Roro Kayla Angelica Priambudi (5025211262)

## Question #1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

### 1A.
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

#### Source Code:
```sh
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Cannot open file: %s\n", filename);
        exit(1);
    }


    int c;
    while ((c = fgetc(file)) != EOF) { 
        freq[c]++; 
    }


    fclose(file);
}


int main() {
    const char* input_filename = "file.txt";
    const char* compressed_filename = "compressed_file.txt";


    // hitung frequency karakter
    unsigned int freq[MAX_CHAR] = {0};
    count_freq(input_filename, freq);


```
#### Explanation:
The given code counts the frequency of characters in a file. It defines a function ``count_freq`` that takes a filename and an array freq as parameters. The freq array is used to store the frequency of each character encountered in the file.

Here's how the code works:
- The count_freq function opens the specified file using ``fopen`` in read mode. If the file cannot be opened, it prints an error message and exits the program.
- It declares a variable c to store the characters read from the file.
- It reads each character from the file using ``fgetc`` in a loop until the end of the file is reached (EOF). Inside the loop, it increments the corresponding element of the freq array based on the ASCII value of the character.
- After reading all the characters from the file, it closes the file using ``fclose``.

In the main function:
- It specifies the input filename as ``file.txt`` and the compressed filename as ``compressed_file.txt``.
- It declares an array freq of unsigned integers with ``size MAX_CHAR`` and initializes all elements to 0.
- It calls the ``count_freq`` function passing the input filename and the freq array as arguments, which counts the frequency of characters in the file and stores the results in the freq array.


### 1B.
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

#### Source Code:
```sh
// struct huffman tree
struct Node {
    char data;
    unsigned freq;
    struct Node *left, *right;
};


// compress file
void compress_file(const char* input_filename, const char* output_filename, char* huffCode[MAX_CHAR]) {
    FILE* input_file = fopen(input_filename, "r");
    if (input_file == NULL) {
        printf("Cannot open input file: %s\n", input_filename);
        exit(1);
    }
    FILE* output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        printf("Cannot create output file: %s\n", output_filename);
        exit(1);
    }
    int c;
    while ((c = fgetc(input_file)) != EOF) {
        char* code = huffCode[c];
        fwrite(code, sizeof(char), strlen(code), output_file);
    }
    fclose(input_file);
    fclose(output_file);
}

```

#### Explanation:
The given code defines a ``struct Node`` representing a node in a Huffman tree, and a function ``compress_file`` for compressing a file using the Huffman algorithm. The purpose of this code is to compress a file using the Huffman algorithm. It reads each character from the input file, retrieves its corresponding Huffman code, and writes the code to the output file. The ``huffCode`` array is assumed to contain the Huffman codes for each character.

Let's break down the code:
- The Node struct represents a node in the Huffman tree. It contains the following members: ``data``, ``freq``, pointer ``left`` and ``right``
- The ``compress_file`` function takes three parameters: the input filename, the output filename, and ``huffCode``, which is an array of strings representing the Huffman codes for each character.
- The function opens the input file using fopen in read mode and checks if it was successfully opened. If the file cannot be opened, it prints an error message and exits the program.
- It opens the output file using fopen in write binary mode ("wb"). If the file cannot be created, it prints an error message and exits the program.
- Inside a while loop, it reads each character from the input file using ``fgetc`` until the end of the file is reached (EOF). For each character, it retrieves the corresponding Huffman code from the ``huffCode`` array.
- It writes the Huffman code to the output file using fwrite. The fwrite function writes the binary data represented by code (which is a string) to the output file. It writes sizeof(char) bytes for each character in the code and writes a total of strlen(code) characters.
- After compressing all the characters in the input file, it closes both the input and output files using fclose.


### 1C.
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

#### Source Code:
```sh
// membuat node baru dalam tree
struct Node* create_node(char data, unsigned freq) {
    struct Node* node = (struct Node*)malloc(sizeof(struct Node));
    node->left = node->right = NULL;
    node->data = data;
    node->freq = freq;
    return node;
}


// menggabungkan dua node yang memiliki frequency paling kecil
struct Node* mergeNode(struct Node* left, struct Node* right) {
    struct Node* node = create_node('\0', left->freq + right->freq);
    node->left = left;
    node->right = right;
    return node;
}


// create huffman 
struct Node* create_huffman(unsigned int freq[MAX_CHAR]) {
    struct Node *nodes[MAX_CHAR];
    int i, j;
    for (i = 0, j = 0; i < MAX_CHAR; i++) {
        if (freq[i] > 0) {
            nodes[j] = create_node(i, freq[i]);
            j++;
        }
    }


    int size = j;
    while (size > 1) {
        struct Node *left = nodes[size - 2];
        struct Node *right = nodes[size - 1];
        struct Node *merged = mergeNode(left, right);
        nodes[size - 2] = merged;
        size--;
    }


    return nodes[0];
}


// fungsi untuk membuat huffman code
void generate_code(struct Node* root, char* code, int depth, char* huffCode[MAX_CHAR]) {
    if (root == NULL) {
        return;
    }


    if (root->left == NULL && root->right == NULL) {
        code[depth] = '\0';
        huffCode[root->data] = strdup(code);
    }


    code[depth] = '0';
    generate_code(root->left, code, depth + 1, huffCode);


    code[depth] = '1';
    generate_code(root->right, code, depth + 1, huffCode);


    if (depth > 0) {
        free(huffCode[root->data]);
    }
}

```

#### Explanation:
The updated code defines functions for creating a Huffman tree and generating Huffman codes. The purpose of this code is to create a Huffman tree and generate Huffman codes for each character based on their frequencies. The Huffman codes are used for compression to represent frequently occurring characters with shorter codes:
- The ``create_node`` function creates a new node in the Huffman tree with the given character and frequency.
- The ``merge_nodes`` function merges two nodes by creating a new node with a null character and a frequency equal to the sum of the frequencies of the input nodes.
- The ``create_huffman_tree`` function creates a Huffman tree based on character frequencies. It iterates over the frequency array, creates nodes for characters with non-zero frequencies, and merges nodes until only one node remains, which becomes the root of the Huffman tree.
- The ``generate_code function`` generates Huffman codes for each character in the Huffman tree. It takes the root of the Huffman tree, an array code to store the current code being generated, the depth of the current node in the tree, and an array of strings huffCode to store the Huffman code for each character.


### 1D.
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi.

#### Source Code:
```sh
// menyimpan huffman tree ke file yang sudah dicompress
void save_huffman(struct Node* root, FILE* file) {
    if (root == NULL) {
        return;
    }


    if (root->left == NULL && root->right == NULL) {
        fputc('1', file);
        fputc(root->data, file);
    } else {
        fputc('0', file);
        save_huffman(root->left, file);
        save_huffman(root->right, file);
    }
}


int main() {
        // hitung jumlah bit setelah dicompress dengan algoritma huffman
        unsigned long long int compressed_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0 && huffCode[i] != NULL) {
                compressed_bits += freq[i] * strlen(huffCode[i]);
            }
        }


```
#### Explanation:
The purpose of this code is to save the Huffman tree structure in the compressed file and calculate the total number of bits required for the compressed file using Huffman coding:
- The save_huffman function saves the structure of the Huffman tree to a compressed file. It recursively traverses the tree and writes '1' followed by the character data for leaf nodes, and '0' for non-leaf nodes.
- In the main function, the variable compressed_bits is used to calculate the total number of bits after compression. It iterates over the characters, checks if they have frequencies and corresponding Huffman codes, and adds the product of the frequency and code length to compressed_bits.


### 1E.
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

#### Source Code:
```sh
       compress_file(input_filename, compressed_filename, huffCode);


        // hitung jumlah bit setelah dicompress dengan algoritma huffman
        unsigned long long int compressed_bits = 0;
        for (int i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0 && huffCode[i] != NULL) {
                compressed_bits += freq[i] * strlen(huffCode[i]);
            }
        }


        printf("total bits original file: %llu\n", original_bits);
        printf("total bits compressed file: %llu\n", compressed_bits);


        // wait child process
        wait(NULL);


        //child process
    } else {
        close(pipe_fd[0]);


        // write frequency karakter ke parent process
        write(pipe_fd[1], freq, sizeof(freq));
        exit(0);
    }
```

#### Explanation:
This code segment is part of the ``main`` function:
- It calls the compress_file function, passing the input filename, compressed filename, and huffCode array as arguments. This initiates the compression process.
- It declares an unsigned long long int variable compressed_bits to store the total number of bits after compression.
- It enters a loop that iterates over each character (represented by i) in the freq array. For each character, it checks if the frequency is greater than 0 and if a corresponding Huffman code exists ``huffCode[i] != NULL``.
- If both conditions are satisfied, it adds the product of the frequency and the length of the Huffman code to the compressed_bits variable. This calculates the total number of bits required to represent the compressed file using Huffman coding.
- It prints the total number of bits in the original file ``original_bits`` and the total number of bits in the compressed file ``compressed_bits`` using printf statements.
- It waits for the child process to finish using the wait function. This ensures that the parent process waits for the child process to complete before exiting.
- In the child process branch (else statement), it closes the read end of the pipe ``pipe_fd[0]``.
- It writes the freq array to the parent process using the write function and the write end of the pipe ``pipe_fd[1]``. This allows the child process to communicate the frequency of characters to the parent process.
- Finally, the child process exits with exit(0), indicating a successful termination.


## Question #2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

### 2A (kalian.c)
Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

#### Source Code:
```sh
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define SHM_SIZE 4096

int main() {
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);
    
    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }
    
    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int*) hasil == (int *) -1) {
        perror("shmat");
        exit(1);
    }

    // Menginisialisasi fungsi random
    srand(time(NULL));

    // Matriks pertama (Ordo 4x2)
    int matriks1[4][2];
    int i, j;
    printf("Matriks pertama:\n");
    for (i = 0; i < 4; i++) {
        printf(" [ ");
        for (j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 5 + 1; // nilai random dari 1-5
            printf("%d ", matriks1[i][j]);
        }
        printf("] \n");
    }

    // Matriks kedua (Ordo 2x5)
    int matriks2[2][5];
    printf("Matriks kedua:\n");
    for (i = 0; i < 2; i++) {
        printf(" [ ");
        for (j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 4 + 1; // nilai random dari 1-4
            printf("%d ", matriks2[i][j]);
        }
        printf("] \n");
    }

    // Perkalian matriks pertama dan matriks kedua
    int k;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            hasil[i][j] = 0;
            for (k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }
    // Output hasil perkalian
    printf("Hasil perkalian:\n");
    for (i = 0; i < 4; i++) {
        printf(" [ ");
    for (j = 0; j < 5; j++) {
        printf("%d ", hasil[i][j]);
    }
    printf("] \n");
    }


    // Melepaskan shared memory dari memori program
    shmdt(hasil);

    return 0;
}

```

#### Explanation:
-Generate a unique key for shared memory using ftok.

-Create a shared memory segment using shmget, specifying the key, size, and permission flags.

-Check if the shared memory creation was successful. If not, display an error message and exit.

-Attach the shared memory segment to the program's memory using shmat, which returns a pointer to the shared memory.

-Check if the attachment was successful. If not, display an error message and exit.

-Initialize the random number generator using srand and the current time.

-Generate and print the first matrix (4x2) with random values from 1 to 5.

-Generate and print the second matrix (2x5) with random values from 1 to 4.

-Perform matrix multiplication by iterating through the matrices and calculating the dot product of corresponding rows and columns.

-Store the result in the shared memory location.

-Print the result matrix obtained from the multiplication.

-Detach the shared memory from the program's memory using shmdt.

-Return 0 to indicate successful program execution.

### 2B (cinta.c)
Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

#### Source Code:
```sh
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHM_SIZE 4096
#define NUM_THREADS 4

struct ThreadData {
    int row;
    int (*matrix)[5];
};

..............

int main() {
    //Calculate CPU time
    clock_t start, finish;
    double cpu_time;
    start = clock();
    
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);

    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int *)hasil == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Menyiapkan data thread
    pthread_t threads[NUM_THREADS];
    struct ThreadData threadData[NUM_THREADS];

    // Membuat thread-thread untuk menghitung faktorial
    for (int i = 0; i < NUM_THREADS; i++) {
        threadData[i].row = i;
        threadData[i].matrix = hasil;

        int rc = pthread_create(&threads[i], NULL, calculateFactorial, (void *)&threadData[i]);
        if (rc) {
            exit(1);
        }
    }

    // Bergabung dengan thread-thread yang telah selesai
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    // Melepaskan shared memory dari memori program
    shmdt(hasil);
    
    
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time: %f s\n", cpu_time);
    return 0;
}

```

#### Explanation:
-Define a structure ThreadData to hold data for each thread. It contains an int variable row and a pointer to a 2D matrix matrix.

-Start measuring CPU time using clock().

-Generate a unique key for shared memory using ftok.

-Get the shared memory segment using shmget, specifying the key, size, and permission flags.

-Check if the shared memory retrieval was successful. If not, display an error message and exit.

-Attach the shared memory segment to the program's memory using shmat, which returns a pointer to the shared memory.

-Check if the attachment was successful. If not, display an error message and exit.

-Create an array of pthread_t threads and an array of ThreadData structures.

-Initialize each ThreadData structure with the corresponding row index and a pointer to the shared matrix.

-Create threads using pthread_create, passing the thread ID, thread attributes, the calculateFactorial function, and the ThreadData structure as arguments.

-Check if the thread creation was successful. If not, exit the program.

-Wait for all threads to finish using pthread_join.

-Detach the shared memory from the program's memory using shmdt.

-Stop measuring CPU time using clock().

-Calculate the CPU time taken by subtracting the start time from the finish time and dividing by CLOCKS_PER_SEC.

-Print the CPU time used.

-Return 0 to indicate successful program execution.

### 2C (cinta.c)
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

#### Source Code:
```sh
void *calculateFactorial(void *arg) {
    struct ThreadData *data = (struct ThreadData *)arg;
    int row = data->row;
    int (*matrix)[5] = data->matrix;
    for (int j = 0; j < 5; j++) {
        unsigned long long factorial = 1;
        int num = matrix[row][j];

        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }

        printf("%llu ", factorial);
    }

    printf("\n");

    pthread_exit(NULL);
}

```

#### Explanation:
-The function receives a void pointer arg, which is then cast to a pointer of type struct ThreadData to access the thread-specific data.

-Retrieve the row index and the pointer to the matrix from the ThreadData structure.

-Iterate through each column of the matrix.

-Initialize a variable factorial to 1 to store the factorial value.

-Retrieve the number from the matrix at the current row and column.

-Calculate the factorial of the number by iterating from 1 to the number and multiplying each value to factorial.

-Print the calculated factorial value.

-Print a newline character to separate the output of each thread.

-Terminate the thread using pthread_exit(NULL).

### 2D (sisop.c)
Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.


#### Source Code:
```sh
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

#define SHM_SIZE 4096

unsigned long long faktorial(unsigned long long n) {
    unsigned long long hasil = 1;
    for (unsigned long long i = 1; i <= n; i++) {
        hasil *= i;
    }
    return hasil;
}

int main() {
    // Calculate CPU time
    clock_t start, finish;
    double cpu_time;
    start = clock();
    
    // Key untuk shared memory
    key_t key = ftok("kalian.c", 42);

    // Membuat segment shared memory
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // Mengaitkan segment shared memory ke memori program
    int (*hasil)[5];  // Pointer ke matriks hasil
    hasil = shmat(shmid, NULL, 0);
    if ((int *)hasil == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Menghitung faktorial dari hasil perkalian
    printf("Faktorial dari hasil perkalian:\n");
    for (int i = 0; i < 4; i++) {
        printf(" [ ");
        for (int j = 0; j < 5; j++) {
            unsigned long long int factorial = 1;
            int num = hasil[i][j];

            for (int k = 1; k <= num; k++) {
                factorial *= k;
            }

            printf("%llu ", factorial);
        }
        printf("] \n");
    }

    // Melepaskan shared memory dari memori program
    shmdt(hasil);
    
    finish = clock();

    // Calculate CPU time 
    cpu_time = ((double) (finish - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time: %f s\n", cpu_time);

    return 0;
}

```

#### Explanation:
-Define a function faktorial that takes an unsigned long long integer n as input and calculates the factorial of n using a loop.

-Start measuring CPU time using clock().

-Generate a unique key for shared memory using ftok.

-Get the shared memory segment using shmget, specifying the key, size, and permission flags.

-Check if the shared memory retrieval was successful. If not, display an error message and exit.

-Attach the shared memory segment to the program's memory using shmat, which returns a pointer to the shared memory.

-Check if the attachment was successful. If not, display an error message and exit.

-Iterate through the matrix and calculate the factorial for each element.

-Print the calculated factorial values.

-Detach the shared memory from the program's memory using shmdt.

-Stop measuring CPU time using clock().

-Calculate the CPU time taken by subtracting the start time from the finish time and dividing by CLOCKS_PER_SEC.

-Print the CPU time used.

-Return 0 to indicate successful program execution.

## Question #4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

### 4A (unzip.c).
Download dan unzip file tersebut dalam kode c bernama unzip.c.
#### Source Code:
```sh
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
  // Download hehe.zip
  pid_t download_pid = fork();
  if (download_pid == 0) {
    execl("/usr/bin/wget", "wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL);
  } else {
    int status;
    waitpid(download_pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Download failed.\n");
      exit(1);
    }
  }

  // Unzip file
  pid_t unzip_pid = fork();
  if (unzip_pid == 0) {
    execl("/usr/bin/unzip", "unzip", "hehe.zip", NULL);
  } else {
    int status;
    waitpid(unzip_pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Unzip process failed.\n");
      exit(1);
    }
  }
  return 0;
}
```
#### Explanation:
The code is a simple program that performs the following tasks:

1. It creates a child process to download a file named "hehe.zip" from a specific URL using the `wget` command.
2. The parent process waits for the download process to complete using the `waitpid` function. If the download process fails (indicated by a non-zero exit status), it prints "Download failed" and exits with an error code.
3. After the download is complete, it creates another child process to unzip the downloaded file using the `unzip` command.
4. The parent process waits for the unzip process to complete using `waitpid`. If the unzip process fails (indicated by a non-zero exit status), it prints "Unzip process failed" and exits with an error code.
5. Finally, the program returns 0 to indicate successful execution.

Overall, this code demonstrates the usage of `fork()` to create child processes, `execl()` to execute external commands, and `waitpid()` to wait for the completion of child processes. It combines downloading and unzipping files using command-line tools (`wget` and `unzip`) within a simple program.

#### Output:
<img src="screenshots/unzip.png" width ="500">

### 4B-E (categorize.c).
b. Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

c. Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file

d. Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

e. Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
Path dimulai dari folder files atau categorized
Simpan di dalam log.txt
ACCESSED merupakan folder files beserta dalamnya
Urutan log tidak harus sama

#### Source Code:
```sh
#include <stdio.h>
#include <dirent.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>
#include <pthread.h>

#define banyakExtension 7
#define panjangExtension 4
#define maxFile 10

typedef struct {
  char name[5];
  int num;
} ExtensionDetail;

char tmpMaxFile[panjangExtension], extensions[banyakExtension][panjangExtension];
ExtensionDetail extensionEntry[banyakExtension + 1];

int customComparator(const void* a, const void* b) {
  ExtensionDetail* ext_count_a = (ExtensionDetail*)a;
  ExtensionDetail* ext_count_b = (ExtensionDetail*)b;
  return ext_count_b->num - ext_count_a->num;
}

void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else wait(&status);
}

void write_log(char* action, char* path) {
  FILE* fp;
  char timestamp[20];

  time_t now = time(NULL);
  strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&now));

  fp = fopen("log.txt", "a");
  fprintf(fp, "%s %s %s\n", timestamp, action, path);
  fclose(fp);
}

void move_file(char* extens, char* src_path, char* dest_path) {
  char mv_argv[500] = "";
  snprintf(mv_argv, sizeof(mv_argv), "mv \"%s\" -t \"%s\"", src_path, dest_path);

  char* mv_file[] = { "bash", "-c", mv_argv, NULL };
  bash_command(mv_file, "/bin/bash");

  char new_path[500] = "";
  strcpy(new_path, extens);
  strcat(new_path, " file : ");
  strcat(new_path, src_path);
  strcat(new_path, " > ");
  strcat(new_path, dest_path);

  write_log("MOVED", new_path);
}

int getNumFiles(char* extension) {
  char command[1000], output[1000];
  FILE* fp;

  char tmp[10] = "*.";
  strcat(tmp, extension);
  if (strcmp(extension, "others") != 0) {
    snprintf(command, sizeof(command), "find files -type f -iname \"%s\" | wc -l", tmp);
  }
  else snprintf(command, sizeof(command), "find files -type f | wc -l");

  fp = popen(command, "r");
  if (fp == NULL) {
    perror("Failed to run command");
    exit(EXIT_FAILURE);
  }
  fgets(output, sizeof(output), fp);
  pclose(fp);

  char* result = strdup(output);
  return atoi(result);
}

void traverse_directory(char* dir_path, char* bufDirectory, char* lowerbufExtension, char* upperBufExtension, int cnt, int numFilePerExtension) {
  struct dirent* entry;
  char sub_path[PATH_MAX];

  DIR* dir = opendir(dir_path);
  if (dir == NULL) {
    perror("Error");
    exit(1);
  }

  while ((entry = readdir(dir)) != NULL && cnt < numFilePerExtension) {
    if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
      snprintf(sub_path, PATH_MAX, "%s/%s", dir_path, entry->d_name);
      write_log("ACCESSED", sub_path);
      traverse_directory(sub_path, bufDirectory, lowerbufExtension, upperBufExtension, cnt, numFilePerExtension);
    }
    else if (entry->d_type == DT_REG) {
      snprintf(sub_path, PATH_MAX, "%s/%s", dir_path, entry->d_name);
      write_log("ACCESSED", sub_path);

      if (strcmp(lowerbufExtension, "others") != 0 && (strstr(entry->d_name, lowerbufExtension) != NULL || strstr(entry->d_name, upperBufExtension) != NULL)) {
        move_file(lowerbufExtension, sub_path, bufDirectory);
        cnt++;
      }
      else if (strcmp(lowerbufExtension, "others") == 0) {
        move_file(lowerbufExtension, sub_path, bufDirectory);
        cnt++;
      }
    }
  }
  closedir(dir);
}

int id_thread;

void* starting_program(void* arg) {
  char* extensionName = (char*)arg;

  if (strcmp(extensionName, "jpg") == 0) id_thread = 1;
  else if (strcmp(extensionName, "txt") == 0) id_thread = 2;
  else if (strcmp(extensionName, "js") == 0) id_thread = 3;
  else if (strcmp(extensionName, "py") == 0) id_thread = 4;
  else if (strcmp(extensionName, "png") == 0) id_thread = 5;
  else if (strcmp(extensionName, "emc") == 0) id_thread = 6;
  else if (strcmp(extensionName, "xyz") == 0) id_thread = 7;
  strcpy(extensionEntry[id_thread - 1].name, extensionName);

  char lowerbufExtension[5] = "";
  snprintf(lowerbufExtension, sizeof(lowerbufExtension), ".%s", extensionName);

  int numFilePerExtension = getNumFiles(extensionName);
  for (int j = 0; j <= numFilePerExtension / maxFile; j++) {
    char bufDirectory[20] = "";
    snprintf(bufDirectory, sizeof(bufDirectory), "categorized/%s", extensionName);
    if (j) {
      snprintf(bufDirectory, sizeof(bufDirectory), "categorized/%s (%d)", extensionName, j);
    }
    printf("bufDirectory %s\n", bufDirectory);

    char upperBufExtension[5] = "";
    for (int len = 0; len < strlen(lowerbufExtension); len++) {
      upperBufExtension[len] = toupper(lowerbufExtension[len]);
    }

    if (!(id_thread - 1)) write_log("MADE", "categorized");
    char* mkdir_argv[] = { "mkdir", "-p", bufDirectory, NULL };
    bash_command(mkdir_argv, "/bin/mkdir");
    write_log("MADE", bufDirectory);

    if (numFilePerExtension < maxFile) {
      traverse_directory("files", bufDirectory, lowerbufExtension, upperBufExtension, 0, numFilePerExtension);
    }
    else {
      if (j != numFilePerExtension / maxFile) {
        traverse_directory("files", bufDirectory, lowerbufExtension, upperBufExtension, 0, maxFile);
      }
      else {
        traverse_directory("files", bufDirectory, lowerbufExtension, upperBufExtension, 0, numFilePerExtension % maxFile);
      }
    }
  }
}

int main() {
  // GET EXTENSIONS
  int idx = 0;
  FILE* ptr = fopen("extensions.txt", "r");
  while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
    strcpy(extensions[idx], tmpMaxFile);
    idx++;
  }
  fclose(ptr);

  pthread_t tid[banyakExtension];

  // 7 EXTENSIONS
  for (int i = 0; i < banyakExtension; i++) {
    // starting_program(extensions[i]);
    pthread_create(&tid[i], NULL, starting_program, extensions[i]);
    int numFilePerExtension = getNumFiles(extensions[i]);
    extensionEntry[i].num = numFilePerExtension;
  }

  for (int i = 0; i < banyakExtension; i++) {
    pthread_join(tid[i], NULL);
  }

  // CATEGORIZING OTHERS
  int numOtherFiles = getNumFiles("others");
  strcpy(extensionEntry[7].name, "others");
  extensionEntry[7].num = numOtherFiles;

  char* mkdir_others[] = { "mkdir", "-p", "categorized/others", NULL };
  bash_command(mkdir_others, "/bin/mkdir");
  write_log("MADE", "categorized/others");
  traverse_directory("files", "categorized/others", "others", "", 0, numOtherFiles);

  // ANSWER NUMBER 4C
  qsort(extensionEntry, banyakExtension + 1, sizeof(ExtensionDetail), customComparator);
  for (int i = banyakExtension; i >= 0; i--) {
    printf("extension_%s : %d \n", extensionEntry[i].name, extensionEntry[i].num);
  }

  return 0;
}
```
#### Explanation:
This code performs the task of categorizing files based on their extensions. Here is a brief explanation of the code:

1. It includes necessary header files and defines constants and a structure for storing extension details.
2. There are utility functions such as `customComparator`, `bash_command`, `write_log`, `move_file`, and `getNumFiles` that handle various operations like sorting, executing shell commands, writing logs, moving files, and getting the number of files with a specific extension.
3. The `traverse_directory` function recursively traverses the directory and categorizes files based on their extensions. It moves the files to the appropriate directories and writes logs for each file movement.
4. The `starting_program` function is the entry point for each thread. It determines the ID of the thread based on the extension and performs the categorization for that extension.
5. In the `main` function, it reads the extensions from a file and creates threads for each extension to start the categorization process.
6. After the categorization is complete, it categorizes the remaining files with the "others" extension and writes logs accordingly.
7. Finally, it sorts the extension details based on the number of files per extension and prints the sorted list.

Overall, the code demonstrates the use of threads, file and directory operations, shell commands execution, logging, and sorting to categorize files into respective directories based on their extensions.

#### Output:
<img src="screenshots/categorize_1.png" width ="500">
<img src="screenshots/categorize_2.png" width ="500">
<img src="screenshots/categorize_3.png" width ="500">


### 4F (logchecker.c).
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.#### Source Code:
```sh
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>

typedef struct {
  char name[20];
  int num;
} FolderDetail;

int customComparator(const void* a, const void* b) {
  const FolderDetail* ext_count_a = (const FolderDetail*)a;
  const FolderDetail* ext_count_b = (const FolderDetail*)b;
  return (int)ext_count_b->num - (int)ext_count_a->num;
}

int getWordFrequency(char* word, char* fileName) {
  char command[1000], output[1000];
  FILE* fp;
  snprintf(command, sizeof(command), "awk -v word=\"%s\" '{for(i=1;i<=NF;i++){if($i==word){count++}}}END{print count}' %s", word, fileName);

  fp = popen(command, "r");
  if (fp == NULL) {
    printf("Failed to run command\n");
    exit(EXIT_FAILURE);
  }
  fgets(output, sizeof(output), fp);
  pclose(fp);

  char* result = strdup(output);
  return atoi(result);
}

void getFoldersName(FolderDetail* folder) {
  FILE* fp;
  char path[1035];
  char command[50] = "ls -d */";

  chdir("categorized");
  fp = popen(command, "r");
  if (fp == NULL) {
    printf("Failed to run command\n");
    exit(1);
  }

  int idx = 0;
  while (fgets(path, sizeof(path) - 1, fp) != NULL) {
    // printf("%s", path);
    int len = strcspn(path, "/\n");
    path[len] = '\0';
    strcpy(folder[idx].name, path);
    idx++;
  }

  pclose(fp);
  chdir("..");
}

int main() {
  char* fileName = "log.txt";
  int banyakExtension = 8;

  // GET FREQUENCY OF ACCESSED
  int accessedWordFrequency = getWordFrequency("ACCESSED", fileName);
  printf("ACCESSED FREQUENCY : %d\n", accessedWordFrequency);

  // GET FREQUENCY OF MADE
  int madeWordFrequency = getWordFrequency("MADE", fileName);
  printf("MADE FREQUENCY : %d\n", madeWordFrequency);

  // GET LIST OF MADE DIRECTORY
  FolderDetail folder[madeWordFrequency - 1];
  getFoldersName(folder);

  // GET EXTENSIONS
  int idx = 0;
  char tmpMaxFile[5];
  FolderDetail extension[banyakExtension];

  FILE* ptr = fopen("extensions.txt", "r");
  while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
    strcpy(extension[idx].name, tmpMaxFile);
    extension[idx].num = 0;
    idx++;
  }
  strcpy(extension[idx].name, "others");
  extension[idx].num = 0;
  fclose(ptr);

  // GET NUMBER OF FILES EVERY FOLDER
  for (int i = 0; i < madeWordFrequency - 1; i++) {
    folder[i].num = 0;
    char buffer[1024];
    FILE* file = fopen(fileName, "r");

    while (fgets(buffer, 1024, file)) {
      if (strlen(folder[i].name) <= 3) {
        if (strstr(buffer, "MOVED") != NULL && strstr(buffer, folder[i].name) != NULL && strstr(buffer, "(") == NULL)
          folder[i].num++;
      }
      else {
        if (strstr(buffer, "MOVED") != NULL && strstr(buffer, folder[i].name) != NULL)
          folder[i].num++;
      }
    }

    if (strstr(folder[i].name, "jpg") != NULL) extension[0].num += folder[i].num;
    else if (strstr(folder[i].name, "txt") != NULL) extension[1].num += folder[i].num;
    else if (strstr(folder[i].name, "js") != NULL) extension[2].num += folder[i].num;
    else if (strstr(folder[i].name, "py") != NULL) extension[3].num += folder[i].num;
    else if (strstr(folder[i].name, "png") != NULL) extension[4].num += folder[i].num;
    else if (strstr(folder[i].name, "emc") != NULL) extension[5].num += folder[i].num;
    else if (strstr(folder[i].name, "xyz") != NULL) extension[6].num += folder[i].num;
    else if (strstr(folder[i].name, "others") != NULL) extension[7].num += folder[i].num;
  }

  // banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending
  qsort(folder, madeWordFrequency - 1, sizeof(FolderDetail), customComparator);
  printf("\n------- Total Per Folder -----\n");
  for (int i = madeWordFrequency - 1; i >= 0; i--) {
    printf("%s : %d\n", folder[i].name, folder[i].num);
  }

  // menghitung banyaknya total file tiap extension, terurut secara ascending
  qsort(extension, banyakExtension, sizeof(FolderDetail), customComparator);
  printf("\n------- Total Per Extension -----\n");
  for (int i = banyakExtension - 1; i >= 0; i--) {
    printf("%s : %d\n", extension[i].name, extension[i].num);
  }
}
```
#### Explanation:
This code analyzes a log file and provides information about the frequency of specific words, the number of files in each directory, and the total number of files per extension. Here's a brief explanation of the code:

1. It includes necessary header files and defines a structure for storing folder details.
2. There is a custom comparator function for sorting folder details based on their frequency.
3. The `getWordFrequency` function calculates the frequency of a given word in a file using the `awk` command.
4. The `getFoldersName` function retrieves the names of directories in the "categorized" directory.
5. In the `main` function:
   - It initializes variables and constants.
   - It calls the `getWordFrequency` function to get the frequency of the "ACCESSED" and "MADE" words in the log file.
   - It calls the `getFoldersName` function to get the list of directories created.
   - It reads the extensions from a file and stores them in the `extension` array.
   - It counts the number of files in each directory by scanning the log file and updates the `num` field of the `folder` and `extension` structures accordingly.
   - It sorts the `folder` and `extension` arrays based on frequency in descending order.
   - It prints the total number of files per folder and per extension.

The code provides insights into the frequency of words in the log file, the number of files in each folder, and the total number of files per extension, allowing for analysis and understanding of the categorized files.

#### Output:
<img src="screenshots/logchecker.png" width ="500">
