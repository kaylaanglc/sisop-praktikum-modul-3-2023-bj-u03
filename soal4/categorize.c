#include <stdio.h>
#include <dirent.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>
#include <pthread.h>

#define banyakExtension 7
#define panjangExtension 4
#define maxFile 10

typedef struct {
  char name[5];
  int num;
} ExtensionDetail;

char tmpMaxFile[panjangExtension], extensions[banyakExtension][panjangExtension];
ExtensionDetail extensionEntry[banyakExtension + 1];

int customComparator(const void* a, const void* b) {
  ExtensionDetail* ext_count_a = (ExtensionDetail*)a;
  ExtensionDetail* ext_count_b = (ExtensionDetail*)b;
  return ext_count_b->num - ext_count_a->num;
}

void bash_command(char* argv[], char* command_execute) {
  pid_t id_child = fork();
  int status;

  if (id_child < 0) {
    printf("Error: Fork Failed\n");
    exit(1);
  }
  else if (id_child == 0) {
    execv(command_execute, argv);
    exit(EXIT_SUCCESS);
  }
  else wait(&status);
}

void write_log(char* action, char* path) {
  FILE* fp;
  char timestamp[20];

  time_t now = time(NULL);
  strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localtime(&now));

  fp = fopen("log.txt", "a");
  fprintf(fp, "%s %s %s\n", timestamp, action, path);
  fclose(fp);
}

void move_file(char* extens, char* src_path, char* dest_path) {
  char mv_argv[500] = "";
  snprintf(mv_argv, sizeof(mv_argv), "mv \"%s\" -t \"%s\"", src_path, dest_path);

  char* mv_file[] = { "bash", "-c", mv_argv, NULL };
  bash_command(mv_file, "/bin/bash");

  char new_path[500] = "";
  strcpy(new_path, extens);
  strcat(new_path, " file : ");
  strcat(new_path, src_path);
  strcat(new_path, " > ");
  strcat(new_path, dest_path);

  write_log("MOVED", new_path);
}

int getNumFiles(char* extension) {
  char command[1000], output[1000];
  FILE* fp;

  char tmp[10] = "*.";
  strcat(tmp, extension);
  if (strcmp(extension, "others") != 0) {
    snprintf(command, sizeof(command), "find files -type f -iname \"%s\" | wc -l", tmp);
  }
  else snprintf(command, sizeof(command), "find files -type f | wc -l");

  fp = popen(command, "r");
  if (fp == NULL) {
    perror("Failed to run command");
    exit(EXIT_FAILURE);
  }
  fgets(output, sizeof(output), fp);
  pclose(fp);

  char* result = strdup(output);
  return atoi(result);
}

void traverse_directory(char* dir_path, char* bufDirectory, char* lowerbufExtension, char* upperBufExtension, int cnt, int numFilePerExtension) {
  struct dirent* entry;
  char sub_path[PATH_MAX];

  DIR* dir = opendir(dir_path);
  if (dir == NULL) {
    perror("Error");
    exit(1);
  }

  while ((entry = readdir(dir)) != NULL && cnt < numFilePerExtension) {
    if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
      snprintf(sub_path, PATH_MAX, "%s/%s", dir_path, entry->d_name);
      write_log("ACCESSED", sub_path);
      traverse_directory(sub_path, bufDirectory, lowerbufExtension, upperBufExtension, cnt, numFilePerExtension);
    }
    else if (entry->d_type == DT_REG) {
      snprintf(sub_path, PATH_MAX, "%s/%s", dir_path, entry->d_name);
      write_log("ACCESSED", sub_path);

      if (strcmp(lowerbufExtension, "others") != 0 && (strstr(entry->d_name, lowerbufExtension) != NULL || strstr(entry->d_name, upperBufExtension) != NULL)) {
        move_file(lowerbufExtension, sub_path, bufDirectory);
        cnt++;
      }
      else if (strcmp(lowerbufExtension, "others") == 0) {
        move_file(lowerbufExtension, sub_path, bufDirectory);
        cnt++;
      }
    }
  }
  closedir(dir);
}

int id_thread;

void* starting_program(void* arg) {
  char* extensionName = (char*)arg;

  if (strcmp(extensionName, "jpg") == 0) id_thread = 1;
  else if (strcmp(extensionName, "txt") == 0) id_thread = 2;
  else if (strcmp(extensionName, "js") == 0) id_thread = 3;
  else if (strcmp(extensionName, "py") == 0) id_thread = 4;
  else if (strcmp(extensionName, "png") == 0) id_thread = 5;
  else if (strcmp(extensionName, "emc") == 0) id_thread = 6;
  else if (strcmp(extensionName, "xyz") == 0) id_thread = 7;
  strcpy(extensionEntry[id_thread - 1].name, extensionName);

  char lowerbufExtension[5] = "";
  snprintf(lowerbufExtension, sizeof(lowerbufExtension), ".%s", extensionName);

  int numFilePerExtension = getNumFiles(extensionName);
  for (int j = 0; j <= numFilePerExtension / maxFile; j++) {
    char bufDirectory[20] = "";
    snprintf(bufDirectory, sizeof(bufDirectory), "categorized/%s", extensionName);
    if (j) {
      snprintf(bufDirectory, sizeof(bufDirectory), "categorized/%s (%d)", extensionName, j);
    }
    printf("bufDirectory %s\n", bufDirectory);

    char upperBufExtension[5] = "";
    for (int len = 0; len < strlen(lowerbufExtension); len++) {
      upperBufExtension[len] = toupper(lowerbufExtension[len]);
    }

    if (!(id_thread - 1)) write_log("MADE", "categorized");
    char* mkdir_argv[] = { "mkdir", "-p", bufDirectory, NULL };
    bash_command(mkdir_argv, "/bin/mkdir");
    write_log("MADE", bufDirectory);

    if (numFilePerExtension < maxFile) {
      traverse_directory("files", bufDirectory, lowerbufExtension, upperBufExtension, 0, numFilePerExtension);
    }
    else {
      if (j != numFilePerExtension / maxFile) {
        traverse_directory("files", bufDirectory, lowerbufExtension, upperBufExtension, 0, maxFile);
      }
      else {
        traverse_directory("files", bufDirectory, lowerbufExtension, upperBufExtension, 0, numFilePerExtension % maxFile);
      }
    }
  }
}

int main() {
  // GET EXTENSIONS
  int idx = 0;
  FILE* ptr = fopen("extensions.txt", "r");
  while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
    strcpy(extensions[idx], tmpMaxFile);
    idx++;
  }
  fclose(ptr);

  pthread_t tid[banyakExtension];

  // 7 EXTENSIONS
  for (int i = 0; i < banyakExtension; i++) {
    // starting_program(extensions[i]);
    pthread_create(&tid[i], NULL, starting_program, extensions[i]);
    int numFilePerExtension = getNumFiles(extensions[i]);
    extensionEntry[i].num = numFilePerExtension;
  }

  for (int i = 0; i < banyakExtension; i++) {
    pthread_join(tid[i], NULL);
  }

  // CATEGORIZING OTHERS
  int numOtherFiles = getNumFiles("others");
  strcpy(extensionEntry[7].name, "others");
  extensionEntry[7].num = numOtherFiles;

  char* mkdir_others[] = { "mkdir", "-p", "categorized/others", NULL };
  bash_command(mkdir_others, "/bin/mkdir");
  write_log("MADE", "categorized/others");
  traverse_directory("files", "categorized/others", "others", "", 0, numOtherFiles);

  // ANSWER NUMBER 4C
  qsort(extensionEntry, banyakExtension + 1, sizeof(ExtensionDetail), customComparator);
  for (int i = banyakExtension; i >= 0; i--) {
    printf("extension_%s : %d \n", extensionEntry[i].name, extensionEntry[i].num);
  }

  return 0;
}
