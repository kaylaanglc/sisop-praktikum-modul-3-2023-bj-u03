#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <wait.h>

typedef struct {
  char name[20];
  int num;
} FolderDetail;

int customComparator(const void* a, const void* b) {
  const FolderDetail* ext_count_a = (const FolderDetail*)a;
  const FolderDetail* ext_count_b = (const FolderDetail*)b;
  return (int)ext_count_b->num - (int)ext_count_a->num;
}

int getWordFrequency(char* word, char* fileName) {
  char command[1000], output[1000];
  FILE* fp;
  snprintf(command, sizeof(command), "awk -v word=\"%s\" '{for(i=1;i<=NF;i++){if($i==word){count++}}}END{print count}' %s", word, fileName);

  fp = popen(command, "r");
  if (fp == NULL) {
    printf("Failed to run command\n");
    exit(EXIT_FAILURE);
  }
  fgets(output, sizeof(output), fp);
  pclose(fp);

  char* result = strdup(output);
  return atoi(result);
}

void getFoldersName(FolderDetail* folder) {
  FILE* fp;
  char path[1035];
  char command[50] = "ls -d */";

  chdir("categorized");
  fp = popen(command, "r");
  if (fp == NULL) {
    printf("Failed to run command\n");
    exit(1);
  }

  int idx = 0;
  while (fgets(path, sizeof(path) - 1, fp) != NULL) {
    // printf("%s", path);
    int len = strcspn(path, "/\n");
    path[len] = '\0';
    strcpy(folder[idx].name, path);
    idx++;
  }

  pclose(fp);
  chdir("..");
}

int main() {
  char* fileName = "log.txt";
  int banyakExtension = 8;

  // GET FREQUENCY OF ACCESSED
  int accessedWordFrequency = getWordFrequency("ACCESSED", fileName);
  printf("ACCESSED FREQUENCY : %d\n", accessedWordFrequency);

  // GET FREQUENCY OF MADE
  int madeWordFrequency = getWordFrequency("MADE", fileName);
  printf("MADE FREQUENCY : %d\n", madeWordFrequency);

  // GET LIST OF MADE DIRECTORY
  FolderDetail folder[madeWordFrequency - 1];
  getFoldersName(folder);

  // GET EXTENSIONS
  int idx = 0;
  char tmpMaxFile[5];
  FolderDetail extension[banyakExtension];

  FILE* ptr = fopen("extensions.txt", "r");
  while (fscanf(ptr, "%s", tmpMaxFile) == 1) {
    strcpy(extension[idx].name, tmpMaxFile);
    extension[idx].num = 0;
    idx++;
  }
  strcpy(extension[idx].name, "others");
  extension[idx].num = 0;
  fclose(ptr);

  // GET NUMBER OF FILES EVERY FOLDER
  for (int i = 0; i < madeWordFrequency - 1; i++) {
    folder[i].num = 0;
    char buffer[1024];
    FILE* file = fopen(fileName, "r");

    while (fgets(buffer, 1024, file)) {
      if (strlen(folder[i].name) <= 3) {
        if (strstr(buffer, "MOVED") != NULL && strstr(buffer, folder[i].name) != NULL && strstr(buffer, "(") == NULL)
          folder[i].num++;
      }
      else {
        if (strstr(buffer, "MOVED") != NULL && strstr(buffer, folder[i].name) != NULL)
          folder[i].num++;
      }
    }

    if (strstr(folder[i].name, "jpg") != NULL) extension[0].num += folder[i].num;
    else if (strstr(folder[i].name, "txt") != NULL) extension[1].num += folder[i].num;
    else if (strstr(folder[i].name, "js") != NULL) extension[2].num += folder[i].num;
    else if (strstr(folder[i].name, "py") != NULL) extension[3].num += folder[i].num;
    else if (strstr(folder[i].name, "png") != NULL) extension[4].num += folder[i].num;
    else if (strstr(folder[i].name, "emc") != NULL) extension[5].num += folder[i].num;
    else if (strstr(folder[i].name, "xyz") != NULL) extension[6].num += folder[i].num;
    else if (strstr(folder[i].name, "others") != NULL) extension[7].num += folder[i].num;
  }

  // banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending
  qsort(folder, madeWordFrequency - 1, sizeof(FolderDetail), customComparator);
  printf("\n------- Total Per Folder -----\n");
  for (int i = madeWordFrequency - 1; i >= 0; i--) {
    printf("%s : %d\n", folder[i].name, folder[i].num);
  }

  // menghitung banyaknya total file tiap extension, terurut secara ascending
  qsort(extension, banyakExtension, sizeof(FolderDetail), customComparator);
  printf("\n------- Total Per Extension -----\n");
  for (int i = banyakExtension - 1; i >= 0; i--) {
    printf("%s : %d\n", extension[i].name, extension[i].num);
  }
}
